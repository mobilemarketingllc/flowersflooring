<?php do_action( 'fl_content_close' ); ?>

	</div><!-- .fl-page-content -->
	<?php

	do_action( 'fl_after_content' );

	if ( FLTheme::has_footer() ) :

		?>
	<footer class="fl-page-footer-wrap"<?php FLTheme::print_schema( ' itemscope="itemscope" itemtype="https://schema.org/WPFooter"' ); ?>  role="contentinfo">
		<?php

		do_action( 'fl_footer_wrap_open' );
		do_action( 'fl_before_footer_widgets' );

		FLTheme::footer_widgets();

		do_action( 'fl_after_footer_widgets' );
		do_action( 'fl_before_footer' );

		FLTheme::footer();

		do_action( 'fl_after_footer' );
		do_action( 'fl_footer_wrap_close' );

		?>
	</footer>
	<?php endif; ?>
	<?php do_action( 'fl_page_close' ); ?>
</div><!-- .fl-page -->
 <div id="agentz-chatbot-f45b473b-a467-481a-969b-3768bfcdb05c"></div>
    <script>
   document.addEventListener('readystatechange', function(event){
        if (event.target.readyState === "complete") {
            var resource = document.createElement('script'); 
            resource.defer = "true";
            resource.src = "https://chatbot.agentz.ai/agentz-chatbot.js?version=1&botUrl=https://chatbot.agentz.ai&title=title&businessAgent=f45b473b-a467-481a-969b-3768bfcdb05c";
            var script = document.getElementsByTagName('script')[0];
            script.parentNode.insertBefore(resource, script);
        }
    }, false);
    </script>	
<?php

wp_footer();

do_action( 'fl_body_close' );

FLTheme::footer_code();

?>
